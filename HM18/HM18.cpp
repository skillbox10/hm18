﻿#include <iostream>
#include <string>

class Player {
private:
	std::string name;
	int score;
	int num;
	std::string* names;
	int* scores;
public:
	Player() {}

	void numOfPlayers() {
		std::cout << "Введите количество игроков\n";
		std::cin >> num;
		//std::cin >> score;

		scores = new int[num];
		names = new std::string[num];
	}
	void fillMassives() {
		for (int i = 0; i < num; i++)
		{
			std::cout << "Введите сначало имя игрока номер " << i + 1 << ", потом количество его очков\n";
			std::cin >> name;
			std::cin >> score;

			scores[i] = score;
			names[i] = name;

			//std::cout << name << " " << score << "\n";
		}
	}
	void Sort() {
		for (int i = 0; i < num; i++) {
			for (int j = 0; j < num - 1; j++) {
				if (scores[j] < scores[j + 1]) {
					int b = scores[j]; // создали дополнительную переменную
					scores[j] = scores[j + 1]; // меняем местами
					scores[j + 1] = b; // значения элементов
					std::string a = names[j];
					names[j] = names[j + 1];
					names[j + 1] = a;
				}
			}
		}
	}

	void printRes() {

		for (int i = 0; i < num; i++)
		{
			std::cout << names[i] << " " << scores[i] << ", ";
		}
	}

	
};

int main()
{

	Player player;
	player.numOfPlayers();
	player.fillMassives();
	player.Sort();
	player.printRes();

}

